#include <typeinfo>
#include <cstdlib>
#include <memory>
#include <cxxabi.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdio.h>

#include "boost/filesystem.hpp"
#include "boost/regex.hpp"

#include "witsml1411.nsmap"
#include "witsml1411H.h"

boost::smatch get_matches(boost::regex rgx, const std::string& uri)
{
	boost::smatch what;
	boost::regex_search(uri, what, rgx);
	return what;
}

std::string demangle(const char* name) {

    int status = -4; // some arbitrary value to eliminate the compiler warning

    // enable c++11 by passing the flag -std=c++11 to g++
    std::unique_ptr<char, void(*)(void*)> res {
        abi::__cxa_demangle(name, NULL, NULL, &status),
        std::free
    };

    return (status==0) ? res.get() : name ;
}

namespace fs = boost::filesystem;
fs::path test_data_path;

template<class T>
void test_one_class(T*(f)(soap*,T*, const char*, const char*))
{
    fs::path p=test_data_path;
    std::string name = demangle(typeid(T).name());

    boost::regex rgx("witsml1411__(obj_)USCORE(.*)$");
	auto captures = get_matches(rgx, name);
	std::string simple_name = captures[1] + captures[2];

    std::cout << simple_name << '\n';

	typedef fs::directory_iterator di;
    p = fs::current_path() / simple_name;
    if (fs::exists(p) && fs::is_directory(p)) {
		for (di d = di(p); d!=di(); d++) {
			fs::path xmlfile = d->path();
			if (fs::exists(xmlfile) && fs::is_regular_file(xmlfile) && ( xmlfile.extension()==".xml") ) {
                std::cout << xmlfile.native() << '\n';

            	struct soap *soap1 = soap_new1 (SOAP_XML_INDENT | SOAP_XML_CANONICAL | SOAP_XML_STRICT);
                std::ifstream fstreamIn(xmlfile.native());
                soap1->is = &fstreamIn;
                T doc;
                int status = (soap_begin_recv(soap1) || (!f(soap1, &doc, NULL, NULL) ) || soap_end_recv(soap1) );
                if(status != SOAP_OK) {
            		std::cout << "read failed on " << xmlfile << ":" << status << std::endl;
                    soap_print_fault(soap1, stderr);                					
				}
                else {
					std::cout  << "deserialized : " << xmlfile << '\n';
                }
                soap_destroy(soap1);
				soap_end(soap1);
				soap_done(soap1);
			}
		}
    }
}

int main(int argC, char ** argV)
{
	if(argC > 1)
		test_data_path = argV[1];
	
	witsml1411__obj_USCOREwell well;

    test_one_class<witsml1411__obj_USCOREwell>(soap_get_witsml1411__obj_USCOREwell);

    return 0;
}
