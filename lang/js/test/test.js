var assert = require("assert"),
    should = require("should"),
	Jsonix = require("../node_modules/jsonix/jsonix").Jsonix,
	witsml1411 = require("../witsml1411").witsml1411;

function read_doc(fileName) {
	console.log(fileName);
	var ctx = new Jsonix.Context([witsml1411]);
	var ser = ctx.createUnmarshaller();
	ser.unmarshalFile(fileName, function(doc) {
		console.log("\tdeserialized without error.");
        // This will be the plural name, same as the collection name in mongo.
        var collectionName = doc.name.localPart;
        // Wack off the 's'
        var singularName = collectionName.substr(0, collectionName.length-1);
        // This will be a collection, but will normally have a length of 1
        var singularCollection = doc.value[singularName];
        for (var i=0; i<singularCollection.length; i++) {
            var dataObject = singularCollection[i];
            var uid = dataObject.uid;
            switch (singularName) {
                case "well":
                    break;
                case "log":
                    var logData = dataObject[logData];
                    dataObject[logData]=null;
                    break;
                case "trajectory":
                    var stations = dataObject.trajectoryStation;
                    dataObject.trajectoryStation = null;
                    var name = dataObject.name;
                    break;
            }
        }
	});
}


var walk = require('walk'),
  fs = require('fs'),
  options,
  walker;

options = {
    followLinks: false
};

var root = "h:/data/energyml/WITSMLSIG_SampleDataSet_1411";
//var root = "../../xml_examples";

walker = walk.walk(root, options);

walker.on("names", function (root, nodeNamesArray, next) {
    for (var i = 0; i < nodeNamesArray.length; i++) {
        var file = nodeNamesArray[i];
        if (file.match(/\.xml/i))
            read_doc(root + "/" + file);
		next();
    }
});

walker.on("file", function (root, fileStats, next) {
    next();
});

walker.on("directories", function (root, dirStatsArray, next) {
  next();
});
